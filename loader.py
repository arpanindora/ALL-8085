def loader(fileNames):
    offset = 0
    fileName = fileNames[0].split('.')[0]
    inputFile = open(fileName + '.linker', 'r')
    code = inputFile.read()
    lines = code.split('\n')
    outFile = open(fileName + '.loader', 'w')
    linkCode = []
    for line in lines:
        if '#' in line:
            tag = line.split(' ')[1]
            newtag = str((int(tag.split('#')[1]) + offset))
            linkCode.append(line.replace(tag, newtag))
        else:
            linkCode.append(line)
    linkCode.append('HLT')
    outFile.write('\n'.join(linkCode))
    outFile.close()
