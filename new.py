import sys
from PyQt4.QtCore import *
from PyQt4.QtGui import *
import main
import os


def simulator():
    finalfile = main.x[0].split('.')[0] + '.loader'
    print('python simulator.py ' + finalfile)
    os.system('python simulator.py ' + finalfile)


def RunClicked():
    finalfile = main.x[0].split('.')[0] + '.loader'
    str1 = ''
    str1 += 'Assembling......\n'
    label.setText(str1)
    main.runass()
    str1 += 'Assembled\nLinking Files......\n'
    label.setText(str1)
    main.runlin()
    str1 += 'Linked Files\nLoading.......\n'
    label.setText(str1)
    main.runload()
    str1 += 'Loaded\nNow you can simulate\n'
    label.setText(str1)



def assemblycode():

    msg = QMessageBox()
    msg.setIcon(QMessageBox.Information)

    try:
        finalfile = main.x[0].split('.')[0] + '.loader'
        msg.setText("This is the sample code")
        with open(finalfile, 'r') as fp:
            code = fp.read()

    except:
        msg.setText("No file run")
        code = 'N/A'

    msg.setDetailedText(code)
    msg.setWindowTitle("Code")

    msg.setStandardButtons(QMessageBox.Close)
    msg.exec_()

def pass1():

    msg = QMessageBox()
    msg.setIcon(QMessageBox.Information)

    try:
        finalfile = main.x[0].split('.')[0] + '.pass1'
        msg.setText("Pass 1")
        with open(finalfile, 'r') as fp:
            code = fp.read()

    except:
        msg.setText("No file run")
        code = 'N/A'

    msg.setDetailedText(code)
    msg.setWindowTitle("Pass 1")

    msg.setStandardButtons(QMessageBox.Close)
    msg.exec_()

def pass2():

    msg = QMessageBox()
    msg.setIcon(QMessageBox.Information)

    try:
        finalfile = main.x[0].split('.')[0] + '.pass2'
        msg.setText("Pass 2")
        with open(finalfile, 'r') as fp:
            code = fp.read()

    except:
        msg.setText("No file run")
        code = 'N/A'

    msg.setDetailedText(code)
    msg.setWindowTitle("Pass 2")

    msg.setStandardButtons(QMessageBox.Close)
    msg.exec_()

def linker():

    msg = QMessageBox()
    msg.setIcon(QMessageBox.Information)

    try:
        finalfile = main.x[0].split('.')[0] + '.linker'
        msg.setText("Linker")
        with open(finalfile, 'r') as fp:
            code = fp.read()

    except:
        msg.setText("No file run")
        code = 'N/A'

    msg.setDetailedText(code)
    msg.setWindowTitle("Linker")

    msg.setStandardButtons(QMessageBox.Close)
    msg.exec_()

def loader():

    msg = QMessageBox()
    msg.setIcon(QMessageBox.Information)

    try:
        finalfile = main.x[0].split('.')[0] + '.loader'
        msg.setText("Loader")
        with open(finalfile, 'r') as fp:
            code = fp.read()

    except:
        msg.setText("No file run")
        code = 'N/A'

    msg.setDetailedText(code)
    msg.setWindowTitle("Loader")

    msg.setStandardButtons(QMessageBox.Close)
    msg.exec_()


def OpenClicked():
    filename = QFileDialog.getOpenFileName(win, 'Open File', '.')
    if filename:
        inputFile = open(filename, "r")
        code = inputFile.read()
        lines = code.split('\n')
        finalfile = lines[0].split('.')[0] + '.loader'
        print(lines[0].split('.')[0])
        print(finalfile)
        main.x = []
        for line in lines:
            if line != '':
                main.x.append(line)

    # print file contents
    with open(filename, 'r') as inputFile:
        code = inputFile.read()
        lines = code.split('\n')
        finalfile = lines[0].split('.')[0] + '.loader'
        print(lines[0].split('.')[0])
        print(finalfile)
        main.x = []
        for line in lines:
            if line != '':
                main.x.append(line)


app = QApplication(sys.argv)
win = QDialog()

run = QPushButton(win)
run.setText("Run")
run.move(300, 100)
run.clicked.connect(RunClicked)

openfile = QPushButton(win)
openfile.setText("Open File")
openfile.move(300, 140)
openfile.clicked.connect(OpenClicked)

label = QLabel(win)
label.move(20, 0)
label.resize(280, 200)

showcode = QPushButton(win)
showcode.setText("Show Sample Code")
showcode.move(300, 20)
showcode.clicked.connect(assemblycode)

showcode = QPushButton(win)
showcode.setText("Pass 1")
showcode.move(20, 240)
showcode.clicked.connect(pass1)

showcode = QPushButton(win)
showcode.setText("Pass 2")
showcode.move(120, 240)
showcode.clicked.connect(pass2)

showcode = QPushButton(win)
showcode.setText("Linker")
showcode.move(230, 240)
showcode.clicked.connect(linker)

showcode = QPushButton(win)
showcode.setText("Loader")
showcode.move(340, 240)
showcode.clicked.connect(loader)


simulate = QPushButton(win)
simulate.setText("Simulate")
simulate.move(300, 60)
simulate.clicked.connect(simulator)


win.resize(480, 300)
win.setWindowTitle("Assembler Linker Loader")
win.show()
sys.exit(app.exec_())
