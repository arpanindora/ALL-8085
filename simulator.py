import sys
from PyQt4.QtCore import *
from PyQt4.QtGui import *

reg = {}
reg['A'] = 0
reg['B'] = 0
reg['C'] = 0
reg['D'] = 0
reg['E'] = 0
reg['H'] = 0
reg['PC'] = 0
# Special Function

reg['SP'] = 0
PC = 0
stack = []

oplen = {}
dbloc = []


def calculatelen():
    inputFile = open('lenopcodes.cf', "r")
    code = inputFile.read()
    lines = code.split('\n')
    for line in lines:
        line = line.lstrip().rstrip()
        if line != '':
            oplen[line.split(' ')[0]] = int(line.split(' ')[1])


calculatelen()
memory = {}


def load():
    inputFile = open(sys.argv[1], "r")
    code = inputFile.read()
    lines = code.split('\n')
    mem = 0
    for line in lines:
        op = line.split(' ')[0].lstrip().rstrip()
        if op != 'DB':
            memory[mem] = line
            mem += oplen[op]
        else:
            memory[mem] = int(line.split(' ')[1].lstrip().rstrip())
            dbloc.append(mem)
            mem += 1

load()

def skipsim():
    pc = 0
    inst = memory[pc]
    opcode = inst.split(' ')[0]

    while opcode != 'HLT':
        simulator(int(reg['PC']))
        pc = int(reg['PC'])
        inst = memory[pc]
        opcode = inst.split(' ')[0]
    simulator(reg['PC'])
    finish.setText('Finished!!')
    return



def simulator(pc=0):
    inst = memory[pc]
    opcode = inst.split(' ')[0]
    strtemp = 'Current Instruction : ' + str(memory[pc])
    print(strtemp)
    ir.setText(strtemp)
    print('Register Values')
    strtemp = 'A : ' + str(reg['A'])
    print(strtemp)
    rega.setText(strtemp)
    strtemp = 'B : ' + str(reg['B'])
    print(strtemp)
    regb.setText(strtemp)
    strtemp = 'C : ' + str(reg['C'])
    print(strtemp)
    regc.setText(strtemp)
    strtemp = 'D : ' + str(reg['D'])
    print(strtemp)
    regd.setText(strtemp)
    strtemp = 'E : ' + str(reg['E'])
    print(strtemp)
    rege.setText(strtemp)
    strtemp = 'H : ' + str(reg['H'])
    print(strtemp)
    regh.setText(strtemp)
    print('Variable Memory Locations')
    memlocs = 'Memory'
    for db in dbloc:
        memlocs += ('\n'+str(db) + ' : ' + str(memory[db]) )
    memstr.setText(memlocs)
    print(memlocs)
    # raw_input("Press Enter to continue...")
    if opcode == 'HLT':
        finish.setText('Finished!!')
        return
    elif opcode == 'JMP':
        nextinst = int(inst.split(' ')[1])
        PC = nextinst
        print(nextinst)
        print(PC)
    elif opcode == 'MVI':
        regvar = inst.split(' ')[1].split(',')[0].lstrip().rstrip()
        reg[regvar] = int(inst.split(' ')[1].split(',')[1].lstrip().rstrip())
        PC = pc + int(oplen[opcode])
    elif opcode == 'ADI':
        reg['A'] = int(reg['A']) + int(inst.split(' ')[1])
        PC = pc + int(oplen[opcode])
    elif opcode == 'STA':
        memloc = int(inst.split(' ')[1])
        memory[memloc] = int(reg['A'])
        PC = pc + int(oplen[opcode])
    elif opcode == 'LDA':
        memloc = int(inst.split(' ')[1])
        reg['A'] = int(memory[memloc])
        PC = pc + int(oplen[opcode])
    elif opcode == 'MOV':
        destreg = inst.split(' ')[1].split(',')[0].lstrip().rstrip()
        srcreg = inst.split(' ')[1].split(',')[1].lstrip().rstrip()
        reg[destreg] = reg[srcreg]
        PC = pc + int(oplen[opcode])
    elif opcode == 'ADD':
        srcreg = inst.split(' ')[1]
        reg['A'] = int(reg['A']) + int(reg[srcreg])
        PC = pc + int(oplen[opcode])
    elif opcode == 'SUI':
        reg['A'] = int(reg['A']) - int(inst.split(' ')[1])
        PC = pc + int(oplen[opcode])
    elif opcode == 'SUB':
        srcreg = inst.split(' ')[1]
        reg['A'] = int(reg['A']) - int(reg[srcreg])
        PC = pc + int(oplen[opcode])
    elif opcode == 'ANI':
        reg['A'] = int(reg['A']) & int(inst.split(' ')[1])
        PC = pc + int(oplen[opcode])
    elif opcode == 'ANA':
        srcreg = inst.split(' ')[1]
        reg['A'] = int(reg['A']) & int(reg[srcreg])
        PC = pc + int(oplen[opcode])
    elif opcode == 'ORI':
        reg['A'] = int(reg['A']) | int(inst.split(' ')[1])
        PC = pc + int(oplen[opcode])
    elif opcode == 'ORA':
        srcreg = inst.split(' ')[1]
        reg['A'] = int(reg['A']) | int(reg[srcreg])
        PC = pc + int(oplen[opcode])
    elif opcode == 'PUSH':
        srcreg = inst.split(' ')[1]
        stack.append(int(reg[srcreg]))
        PC = pc + int(oplen[opcode])
    elif opcode == 'POP':
        srcreg = inst.split(' ')[1]
        reg[srcreg] = stack.pop()
        PC = pc + int(oplen[opcode])
    elif opcode == 'JNZ':
        nextinst = int(inst.split(' ')[1])
        if int(reg['A']) != 0:
            PC = nextinst
        else:
            PC = pc + int(oplen[opcode])
    elif opcode == 'JZ':
        nextinst = int(inst.split(' ')[1])
        if int(reg['A']) == 0:
            PC = nextinst
        else:
            PC = pc + int(oplen[opcode])
    elif opcode == 'JP':
        nextinst = int(inst.split(' ')[1])
        if int(reg['A']) > 0:
            PC = nextinst
        else:
            PC = pc + int(oplen[opcode])
    reg['PC'] = PC
    regpc.setText('PC: ' + str(PC))


def callback():
    simulator(int(reg['PC']))


app = QApplication(sys.argv)
win = QDialog()
# IR
ir = QLabel(win)
ir.move(20, 20)
ir.resize(460, 20)
ir.setText('Current Instruction: ')

# Registers
registers = QLabel(win)
registers.move(20, 60)
registers.resize(460, 20)
registers.setText('Register values')

rega = QLabel(win)
rega.move(20, 80)
rega.resize(80, 20)
rega.setText('A: ')

regb = QLabel(win)
regb.move(80, 80)
regb.resize(80, 20)
regb.setText('B: ')

regc = QLabel(win)
regc.move(140, 80)
regc.resize(80, 20)
regc.setText('C: ')

regd = QLabel(win)
regd.move(200, 80)
regd.resize(80, 20)
regd.setText('D: ')

rege = QLabel(win)
rege.move(260, 80)
rege.resize(80, 20)
rege.setText('E: ')

regh = QLabel(win)
regh.move(320, 80)
regh.resize(80, 20)
regh.setText('H: ')

# PC
regpc = QLabel(win)
regpc.move(380, 80)
regpc.resize(80, 20)
regpc.setText('PC: ')

memstr = QLabel(win)
memstr.move(20, 120)
memstr.resize(80, 380)
memstr.setText('Memory Used: ')

meters = QLabel(win)
meters.move(20, 520)
meters.resize(80, 20)

finish = QLabel(win)
finish.move(20, 560)
finish.resize(80, 20)

run = QPushButton(win)
run.setText("Simulate")
run.move(450, 500)
run.clicked.connect(callback)

skip = QPushButton(win)
skip.setText("Skip Simulation")
skip.move(450, 450)
skip.clicked.connect(skipsim)

win.resize(600, 600)
win.setWindowTitle("Simulator")
win.show()
sys.exit(app.exec_())
